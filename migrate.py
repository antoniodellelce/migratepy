#!/usr/bin/env python
#
# File:         migrate.py
# Created:      1422 140713
# Description:  description for migrate.py
#
# since the original script has been lost, I am rewriting this in python
#

import migrate_core


#
# main function
#

def main(args):
    m = migrate_core.migrate()

    m.initial_migrate()

    for item in m.all_projects:
      print item
    

if __name__ == '__main__':
    import sys
    main(sys.argv)

## EOF ##

