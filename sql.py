#!/usr/bin/env python
#
# File:         sql.py
# Created:      1249 270713
# Description:  description for sql.py
#
#

import sqlite3

###

class schema:
   '''temp claass for holding schema definitions'''

   def dest_schema(self):

   a = '''
CREATE TABLE project_paths
(
  id                    integer primary key autoincrement,
  path                  varchar(256)
);
CREATE TABLE project_types
(
  id                    integer primary key autoincrement,
  name                  varchar(32)
);
CREATE TABLE projects
(
   id                   integer primary key autoincrement,
   name                 varchar(128),
   type_id              integer,
   path_id              integer,
   save_date            varchar(128),
   save_time            varchar(128),
   class                varchar(128),
   flags                numeric
);
CREATE INDEX i_projects_1 on projects(type_id);
CREATE UNIQUE INDEX i_projects_u1 on projects(name);
CREATE UNIQUE INDEX i_projects_u2 on projects(path_id);
'''

   def origin_schema(self):
   a = '''
'''

###

class projectsdb:
   '''db interface'''

## EOF ##

