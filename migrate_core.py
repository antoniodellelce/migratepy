#!/usr/bin/env python
#
# File:         migrate.py
# Created:      0215 140713
# Description:  description for migrate.py
#
#

### imports ####

import os
import re

## container from projects straight out of the old format ##

class rawProject:
   '''project container'''

   def __init__(self, name, path, saved_time, saved_date, project_type = 'project', project_class = None):
     if name is None:
       raise Exception('name cannot be empty')

     self.name = name
     self.path = path
     self.save_time = saved_time
     self.save_date = saved_date
     if project_type is None:
       self.project_type = 'project'
     else:
       self.project_type = project_type

     self.project_class = project_class

   def __str__(self):
     try:
       return_str = self.name + ';' + self.save_time + ';' + self.save_date + ';' + self.project_type + ';' + self.path
     except TypeError:
       if self.name is None:
         print 'name'
         return ''

       if self.save_type is None:
         print 'save_type'
         return ''

       if self.save_date is None:
         print 'save_date'
         return ''

     if self.project_class is None:
       return return_str
     else:
       return return_str + ';' + self.project_class

## basic migration class ##
    
class migrate:

  def __init__(self):
    self.all_projects = []

  def initial_migrate(self):
    sd = os.environ["SRCCONFIG"] + "/" + "savedirs"

    listDir = os.listdir(sd)

    for entry in listDir:
      pname = None
      saved_time = None
      saved_date = None
      ppath = None
      pclass = None
      ptype = None

      fp = sd + '/' + entry

      fh = open(fp)
      lines = fh.readlines()
 
      for line in lines:
        buf1 = re.search('^#', line)
        if buf1 is not None:
          continue

        a = re.split('=', line)
        a_val = re.split('"', a[1])

        if a[0] == "pname":
          pname = a_val[1]

        if a[0] == "saved_time":
          saved_time = a_val[1]

        if a[0] == "saved_date":
          saved_date = a_val[1]

        if a[0] == "ptype":
          ptype = a_val[1]

        if a[0] == "fullpath":
          ppath = a_val[1]

        if a[0] == "pclass":
          pclass = a_val[1]

      rp = rawProject(pname, ppath, saved_time, saved_date, ptype, pclass)  
      self.all_projects.append(rp)


## EOF ##

